import { Component } from '@angular/core';
import { UserInformationService } from './services/user-information.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  constructor(private userInformationService: UserInformationService){}

  ngOnInit(){
  }
}
