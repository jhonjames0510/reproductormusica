import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlayListComponent } from './components/play-list/play-list.component';
import { PrincipalComponent } from './components/principal/principal.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/playlist'},
  { path: 'playlist', component: PrincipalComponent},
  { path: 'playlist/:playListId', component: PlayListComponent},
  { path: '**', redirectTo: '/user'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
