import { Component, OnInit } from '@angular/core';
import { UserInformationService } from 'src/app/services/user-information.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.sass']
})
export class PrincipalComponent implements OnInit {

  arrPlayList: any[];
  infoUsuario: any;
  arrTopArtists: any[];

  constructor(private userInformationService: UserInformationService) { }

  async ngOnInit(){
    this.userInformationService.getInfo()
    .then(response => this.infoUsuario = response)
    .catch(error => console.log(error));

    this.userInformationService.getTopArtists()
    .then(response => this.arrTopArtists = response['items'])
    .catch(error => console.log(error));

    try{
      const response = await this.userInformationService.getPlayList();
      this.arrPlayList = response['items'];
    }catch(error){
      console.log(error);
    }
  }

}
