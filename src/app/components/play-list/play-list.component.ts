import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserInformationService } from 'src/app/services/user-information.service';

@Component({
  selector: 'app-play-list',
  templateUrl: './play-list.component.html',
  styleUrls: ['./play-list.component.sass']
})
export class PlayListComponent implements OnInit {

  url:string;
  arrTracks: any[];
  infoPlayList: any;
  trackId: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private userInformationService: UserInformationService
  ) { 
    this.trackId = "1BiZIPwNd3yLP9KBoUqMRY";
    this.url = `https://open.spotify.com/embed/track/${this.trackId}?theme=0`;
  }

  ngOnInit(){
    this.activatedRoute.params.subscribe(async params =>{
      const response = await this.userInformationService.getPlayListById(params.playListId);
      this.infoPlayList = response;
    });

    this.activatedRoute.params.subscribe(async params =>{
      const response = await this.userInformationService.getTraksById(params.playListId);
      this.arrTracks = response['items'];
    });
  }

  onClick(tId){
    this.trackId = tId;+
    console.log(tId);
    this.url = `https://open.spotify.com/embed/track/${this.trackId}?theme=0`;
    console.log(this.trackId);
    console.log(this.url);
  }

}
