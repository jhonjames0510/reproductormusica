import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserInformationService {

  baseUrl: string;
  tokenUser: string;
  tokenPalyList: string;

  constructor(private httpCliente: HttpClient) {
    this.baseUrl = 'https://api.spotify.com/v1';
    this.tokenUser = 'BQCsQ3vVpVRFyYHNu7dceT8faqQl--hEGzrlnpGVnHGeacY6pU6V7emXv0Axq0Q-1fZy7P5HElyDHmTXVY579qfKy3t_c_S4FqNhogwOwJGgDoNxMZ2OcgaVoK5hGeCapa8d3csDcsXRByR7KO7Zs13hprt6mkB-eO4xIj8bGIdBaNkPUKNGyjkveUI1XQSLR6UF8PZsqJS1dGRMEtqzSTFtkZn72Ar292ES_qN6uqPUMLZJQUtPckWs9zu1WCvN0ib6Gr2ELiQeUPl0CWUb_w';
    this.tokenPalyList = 'BQBQQQSg2hsiz0jEtkJH4HKcLg35V90CXgLGy5dKoWof5y53fPl588CTxikSTKipSOmM81uM2r58AzPReJkWVzxifP9EO62Ra_Yv0fDznxWn5YsqfWGzbcBm413IVbUQzeXbVVCjxf5CJLA9PaYR5N2pU6Ol5vMbLv18RGOBmHrex-4GKt0u9fVIrM7qAvetNF1FO_Of6J0JvG5GCeLVZsoiR8m8mOkZRQlBJXdjt1zFkd0l6JKC29-HV5l9hv-FsNS_b8vbeFld0vXd0MhC_A';
  }

  getInfo(): Promise<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.tokenUser}`
      })
    }
    return this.httpCliente.get<any>(`${this.baseUrl}/me`, httpOptions).toPromise();
  }

  getPlayList(): Promise<any[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.tokenPalyList}`
      })
    }
    return this.httpCliente.get<any[]>(`${this.baseUrl}/me/playlists`, httpOptions).toPromise();
  }

  getPlayListById(playListId): Promise<any[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.tokenPalyList}`
      })
    }
    return this.httpCliente.get<any>(`${this.baseUrl}/playlists/${playListId}` , httpOptions).toPromise();
  }

  getTraksById(playListId): Promise<any[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.tokenPalyList}`
      })
    }
    return this.httpCliente.get<any>(`${this.baseUrl}/playlists/${playListId}/tracks` , httpOptions).toPromise();
  }

  getTopArtists(): Promise<any[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.tokenUser}`
      })
    }
    return this.httpCliente.get<any>(`${this.baseUrl}/me/top/artists?limit=4` , httpOptions).toPromise();
  }



}
